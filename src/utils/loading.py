import sys
import time


def loading(param, waiting_time):
    """loading animation in shell"""
    display = param
    # print(display)
    if isinstance(param, str):
        match param:
            case "Save":
                display = "Save"
            case "Reset":
                display = "Reset"

    loading_parts = ["[■□□□□□□□□□]", "[■■□□□□□□□□]", "[■■■□□□□□□□]", "[■■■■□□□□□□]", "[■■■■■□□□□□]",
                     "[■■■■■■□□□□]", "[■■■■■■■□□□]", "[■■■■■■■■□□]", "[■■■■■■■■■□]", "[■■■■■■■■■■]"]
    count = len(loading_parts)
    print(display)
    for i in range(count):
        time.sleep(waiting_time)
        sys.stdout.write("\r" + loading_parts[i % count])
        sys.stdout.flush()
    print("\n")

"""
Some methods to build a generic Menu class
"""
from src.app.sub_app_component import SubAppComponent

BACK_TO_MENU = True


def while_input(msg, condition, **kwargs):
    """
    Ask a user an input
    with specific msg
    & condition about the input validity
    until input is valid
    kwargs : quit_entry_array
    :param msg:
    :param condition:
    :return:
    """
    entry = input(msg)
    while not condition(entry, **kwargs):
        print('----------')
        entry = input(msg)
    if user_input_quit_entry(entry, kwargs.get('quit_entry_array')):
        print('exiting the app...')
        exit()
    if entry in ['menu']:
        entry = 'menu'
    return entry


def user_input_quit_entry(entry, quit_entry_array):
    """
    User can always leave the app with one of the following command : q, exit, quit
    :param entry:
    :param quit_entry_array:
    :return:
    """
    return entry in quit_entry_array


def exit_or_back_menu(entry, quit_entry_array, menumenumenu):
    # if entry in menumenumenu:
    #     print('hes in menu etr')
    #     return 'menu'
    # else:
    #     return user_input_quit_entry(entry, quit_entry_array)
    # print('EXIT OR BACK ', entry)
    # return ('RETURN EXIT OR BACK ' + entry)
    return user_input_quit_entry(entry, quit_entry_array) or entry in menumenumenu


# def exit_app(func):
#     """
#     :param func:
#     :return:
#     """
#     def wrapper(*args):
#         return func(*args)
#
#     return wrapper

def back_to_menu(func):
    """
    Check quit or back to menu user entry
    in every sub app (decorator on main_app.run)
    :param func:
    :return:
    """
    def wrapper(*args):
        func(*args)
        back_to_menu_input = while_input(
            msg='quit or menu ?',
            condition=exit_or_back_menu,
            quit_entry_array=SubAppComponent.EXIT_ENTRY_VALUES,
            menumenumenu=['menu', 'Menu']
        )
        return back_to_menu_input
    return wrapper

"""
Menu module
"""
from src.app.sub_app_component import SubAppComponent
from src.app.menu.utils_menu import while_input, user_input_quit_entry


class Menu(SubAppComponent):
    """
    Menu class :
    1 - Option1
    2 - Option2
    ...
    input choice ?
    :return: the user choice
    """
    def __init__(self, menu_options):
        super().__init__()
        self.__menu_options = menu_options
        self.__user_entry = None

    def display(self):
        """
        display the menu:
        1 - Option1
        2 - Option2
        ...
        input choice ?
        :return: the user choice
        """
        for key, action in self.__menu_options.items():
            print(f'{key} - {action.__name__}')

        self.__user_entry = while_input(msg=f'action -> { *SubAppComponent.EXIT_ENTRY_VALUES, } to quit ? ',
                                        condition=self.valid_input,
                                        menu_entry=self.__menu_options,
                                        quit_entry_array=SubAppComponent.EXIT_ENTRY_VALUES)
        choice = self.__menu_options.get(int(self.__user_entry))
        # return self.__menu_options.get(int(
        #     while_input(msg='action (type exit to quit)?',
        #                 condition=self.valid_input,
        #                 menu_entry=self.__menu_options,
        #                 quit_entry_array=self.exit_action)
        # ))
        print(f'choice : {choice.__name__}')
        return choice

    @staticmethod
    def valid_input(entry, **kwargs):
        """
        valid user input for this specific menu
        :param entry:
        :param kwargs:
        :return:
        """
        return Menu.valid_numeric_input(
            entry,
            kwargs.get('menu_entry')
        ) if entry.isnumeric() else Menu.valid_exit_input(entry, kwargs.get('quit_entry_array'))

    @staticmethod
    def valid_numeric_input(entry, menu_entry):
        """
        entry is a number & is in the menu option list
        :param entry: user input
        :param menu_entry: app full menu
        :return:
        """
        # check = 0 < int(entry) <= len(menu_entry)
        # return check, f'{"not in list " if not check else f"go{entry}"}'
        return 0 < int(entry) <= len(menu_entry)

    @staticmethod
    def valid_exit_input(entry, quit_entry_array):
        """
        Everywhere in the app, u can type exit, q or quit to exit the app
        :param entry:
        :param quit_entry_array:
        :return:
        """
        return entry in quit_entry_array

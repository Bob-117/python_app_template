"""
Main app module
"""
from .menu import Menu
from .menu.utils_menu import back_to_menu
from .sub_app import Help, Template
from ..utils.loading import loading


class MainApp:
    """
    Main app class
    """

    def __init__(self, name):
        self.__name = name

    @back_to_menu
    def run_main_app(self):
        """
        create a menu
        display it
        display sub app
        run it and back to menu or quit from sub app
        :return:
        """
        menu = Menu(menu_options={1: Help, 2: Template})
        sub_app = menu.display()
        loop_on_menu = sub_app().display()
        return loop_on_menu

    @property
    def name(self):
        return self.__name

"""
Sub_app template module
"""
from src.app.menu.utils_menu import while_input, user_input_quit_entry
from src.app.sub_app_component import SubAppComponent
from src.utils.loading import loading


class Template(SubAppComponent):
    """
    Sub_app template class
    """
    @staticmethod
    def display():
        print('This is a python app template\n'
              'do what u want here !')
        loading('We can play a game .... :^) \n ', .1)
        age = 117117117117117
        for i in range(15):
            guess = input(f'guess my age : {15 - i} try :')
            if guess.isnumeric():
                if guess == int(age):
                    print('SUCCESS')
        print('Nope, see u later ! :^)')


    # def send_to_main_app(self):
    #     ...

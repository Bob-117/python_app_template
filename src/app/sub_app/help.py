"""
Help module
"""
from src.app.menu.utils_menu import while_input, user_input_quit_entry
from src.app.sub_app_component import SubAppComponent


class Help(SubAppComponent):
    """
    Help class
    """

    @staticmethod
    def display():
        print(u'** Welcome ! \U0001F40D **\n'
              'This is a python app template\n'
              'with language selection (later) and generic menu\n'
              'feel free to use it, have fun ! \U0001F918')


    # @staticmethod
    # @exit_app
    # def haha(self):
    #     print('haha')

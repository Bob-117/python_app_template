from src import MainApp
from src.utils.loading import loading

if __name__ == '__main__':
    app = MainApp(name='Python app template')
    print(f'--> starting {app.name} -->')
    loop = app.run_main_app()
    while loop == 'menu':
        loading('back to menu', .1)
        loop = app.run_main_app()


